import yaml
from appJar import gui
import numpy as np
import threading
import time
import os
import tempfile
import cv2
import datetime
import glob
import traceback

SETTINGS_FILE = 'crosshair_app_settings.yaml'


class YamlSettings(object):

    def __init__(self, settings_path):

        self.settings_path = settings_path
        if os.path.isfile(self.settings_path):
            with open(self.settings_path) as stream:
                self.settings = yaml.safe_load(stream)
        else:
            self.settings = dict()
            with open(self.settings_path, 'w') as stream:
                yaml.safe_dump(self.settings, stream)

    def get(self, item, default=None):
        try:
            return self.settings[item]
        except KeyError:
            return default

    def set(self, key, value):
        self.settings[key] = value
        with open(self.settings_path, 'w') as stream:
            yaml.safe_dump(self.settings, stream)


class CameraThread(threading.Thread):
    def __init__(self, *argv):
        super(CameraThread, self).__init__(*argv)
        self._stop_event = threading.Event()

    def run(self):
        global app_settings

        cap = cv2.VideoCapture(app_settings.get("Camera"))
        cv2.namedWindow('Camera', cv2.WINDOW_KEEPRATIO)
        while not self.stopped():
            try:
                ret, frame = cap.read()
                if app_settings.get("Enable Crosshair"):
                    color = (100, 100, 100, 100)
                    vertical_size = frame.shape[0]
                    horizontal_size = frame.shape[1]
                    vertical_pos = int(vertical_size/2) + app_settings.get("Crosshair Y Position")
                    horizontal_pos = int(horizontal_size/2) + app_settings.get("Crosshair X Position")
                    cv2.line(frame, (0, vertical_pos), (horizontal_size, vertical_pos), color)
                    cv2.line(frame, (horizontal_pos, 0), (horizontal_pos, vertical_size), color)
                    if app_settings.get("Enable Crosshair Center"):
                        cv2.circle(frame, (horizontal_pos, vertical_pos),
                                   app_settings.get("Crosshair Center Radius"), color)
                cv2.imshow('Camera', frame)
                cv2.waitKey(int(1000/30.0))
            except Exception:
                traceback.print_exc()
                self.stop()  # Stop immediately
                app.queueFunction(press_start_stop)
                app.queueFunction(show_popup, traceback.format_exc(), error=True)
        cap.release()
        cv2.destroyWindow('Camera')

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()


def press_start_stop():
    global app, camera_thread
    if app.getButton('Start') == 'Start':
        app.setButton("Start", "Stop")
        update_settings()
        camera_thread = CameraThread()
        camera_thread.start()
    elif app.getButton('Start') == 'Stop':
        app.setButton("Start", "Start")
        camera_thread.stop()


def show_popup(text, error=False):
    if error:
        app.errorBox('Error', text)
    else:
        app.infoBox('Info', text)


def exit_check():
    global camera_thread
    if camera_thread is not None:
        camera_thread.stop()
    print("Exiting...")
    return True


def list_file_info():
    show_popup('File Info')


def update_settings():
    save_ui_settings()


def save_ui_settings():
    global app, app_settings
    for text_entry in []:
        app_settings.set(text_entry, app.getEntry(text_entry))
    for spinbox in ["Camera", "Crosshair X Position", "Crosshair Y Position", "Crosshair Center Radius"]:
        app_settings.set(spinbox, int(app.getSpinBox(spinbox)))
    for checkbox in ["Enable Crosshair", "Enable Crosshair Center"]:
        app_settings.set(checkbox, app.getCheckBox(checkbox))


if __name__ == '__main__':
    app_settings = YamlSettings(SETTINGS_FILE)
    camera_thread = None
    with gui("Camera Crosshair", useTtk=True, showIcon=False) as app:
        app.setSize(300, 400)
        app.setStopFunction(exit_check)

        app.addLabelSpinBoxRange("Camera", 0, 10)
        app.setSpinBox("Camera", str(app_settings.get("Camera", default=0)))

        app.addCheckBox("Enable Crosshair")
        app.setCheckBox("Enable Crosshair", app_settings.get("Enable Crosshair", default=True))
        app.setCheckBoxChangeFunction("Enable Crosshair", update_settings)

        app.addLabelSpinBoxRange("Crosshair X Position", -2000, 2000)
        app.setSpinBox("Crosshair X Position", str(app_settings.get("Crosshair X Position", default=0)))
        app.setSpinBoxChangeFunction("Crosshair X Position", update_settings)

        app.addLabelSpinBoxRange("Crosshair Y Position", -2000, 2000)
        app.setSpinBox("Crosshair Y Position", str(app_settings.get("Crosshair Y Position", default=0)))
        app.setSpinBoxChangeFunction("Crosshair Y Position", update_settings)

        app.addCheckBox("Enable Crosshair Center")
        app.setCheckBox("Enable Crosshair Center", app_settings.get("Enable Crosshair Center", default=True))
        app.setCheckBoxChangeFunction("Enable Crosshair Center", update_settings)

        app.addLabelSpinBoxRange("Crosshair Center Radius", 1, 200)
        app.setSpinBox("Crosshair Center Radius", str(app_settings.get("Crosshair Center Radius", default=10)))
        app.setSpinBoxChangeFunction("Crosshair Center Radius", update_settings)

        app.buttons(["Start", "Exit"], [press_start_stop, app.stop])
